package com.spring.crud;

import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.spring.crud.dao.StudentDAO;
import com.spring.crud.entity.Student;

@SpringBootApplication
public class CrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudApplication.class, args);
	}
		@Bean
		public CommandLineRunner commandLineRunner(StudentDAO studentDAO) {
			// Lamda Expression
			return runner ->	{
//				createStudent(studentDAO);
				createMultipleStudent(studentDAO);
				
//				readStudent(studentDAO);
//				queryForStudent(studentDAO);
//				queryForLastName(studentDAO);
				
//				updateStudent(studentDAO);
				
//				deleteStudent(studentDAO);
//				deleteAllStudents(studentDAO);
				
			};
			
		}
		
		private void deleteAllStudents(StudentDAO studentDAO) {
			System.out.println("Deleting all students");
			int numRowsDeleted = studentDAO.deleteAll();
			System.out.println("Deleted row count "+numRowsDeleted);

		}
		private void deleteStudent(StudentDAO studentDAO) {
			int studentId = 6;
			System.out.println("Deleting Student id "+ studentId);
			studentDAO.delete(studentId);
		}
		private void updateStudent(StudentDAO studentDAO) {
			int studentId = 3;
			System.out.println("Getting Student with id "+studentId);
			Student myStudent = studentDAO.findById(studentId);
			myStudent.setFirstName("Scoby");
			studentDAO.update(myStudent);
			System.out.println("Update Student : "+ myStudent);
			
		}
		private void queryForLastName(StudentDAO studentDAO) {
			List<Student> theStudent = studentDAO.findByLastName("Pandi");
			for(Student tempStudent : theStudent) {
				System.out.println(tempStudent);
			}
		}
		private void queryForStudent(StudentDAO studentDAO) {
			List<Student> theStudents = studentDAO.findAll();
			for(Student tempStudent:theStudents) {
				System.out.println(tempStudent);
			}
		}
		private void readStudent(StudentDAO studentDAO) {
			//	Create a Student Object
			Student tempStudent = new Student("Zayn", "Dinesh", "dinesh@gmail.com");
			//	Save the Student Object
			studentDAO.save(tempStudent);
			//	display the id of the saved student Object
			int theId = tempStudent.getId();
			System.out.println("Saved Student Id : "+ theId);
			//	Retrieve student based on the id: Primary key
			System.out.println("Retrieve student Id : "+ theId);
			Student myStudent = studentDAO.findById(theId);
			//	Display student
			System.out.println("Found the Student "+ myStudent);
		}
		public void createMultipleStudent(StudentDAO studentDAO) {
			//	create students objects
			Student tempStudent1 = new Student("Arun", "Pandi", "arunpandi@gmail.com");
			Student tempStudent2 = new Student("Mathi", "Vanan", "mathivanan@gmail.com");
			Student tempStudent3 = new Student("Manikanda", "Prabhu", "manikandaprabhu@gmail.com");
			
			//	Save the student Object
			studentDAO.save(tempStudent1);
			studentDAO.save(tempStudent2);
			studentDAO.save(tempStudent3);

		}
		
		
		private void createStudent(StudentDAO studentDAO) {
			// create the student object
			System.out.println("Creating new Student object...");
			Student tempStudent = new Student("Jhonny", "Depp", "jhonnydepp@gmail.com");
			
			
			//	save the student object
			System.out.println("Saving the student ...");
			studentDAO.save(tempStudent);
			
			//	display the student object
			System.out.println("Saved student. Generate id : "+tempStudent.getId());
		}


}
